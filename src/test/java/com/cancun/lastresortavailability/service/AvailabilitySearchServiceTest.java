package com.cancun.lastresortavailability.service;

import static org.apache.commons.lang3.RandomUtils.nextLong;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.ReflectionTestUtils.setField;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.cancun.lastresortavailability.configuration.BookingConfiguration;
import com.cancun.lastresortavailability.domain.entity.Availability;
import com.cancun.lastresortavailability.repository.AvailabilityRepository;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(initializers = ConfigDataApplicationContextInitializer.class)
@EnableConfigurationProperties(value = BookingConfiguration.class)
public class AvailabilitySearchServiceTest {

    @InjectMocks
    private AvailabilitySearchService service;

    @Mock
    private AvailabilityRepository repository;

    @Autowired
    private BookingConfiguration configuration;

    @Test
    public void searchAvailability_ok() {
        setField(service, "configuration", configuration);

        final Page<Availability> availabilities = mock(Page.class);
        when(repository.findAllByDayAfterOrderByDayAsc(any(), any())).thenReturn(availabilities);

        final Page<Availability> availableDays = service.getAvailableDays();

        assertNotNull(availableDays);
        assertNotNull(availableDays.getContent());
        assertNotNull(availableDays.getTotalElements());
    }
}
