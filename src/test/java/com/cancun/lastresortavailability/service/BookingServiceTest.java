package com.cancun.lastresortavailability.service;

import static com.cancun.lastresortavailability.fixture.Fixture.make;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.ReflectionTestUtils.setField;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.cancun.lastresortavailability.configuration.BookingConfiguration;
import com.cancun.lastresortavailability.domain.entity.Availability;
import com.cancun.lastresortavailability.domain.entity.Booking;
import com.cancun.lastresortavailability.domain.entity.User;
import com.cancun.lastresortavailability.domain.message.BookingMessage;
import com.cancun.lastresortavailability.fixture.Fixture;
import com.cancun.lastresortavailability.repository.AvailabilityRepository;
import com.cancun.lastresortavailability.repository.BookingRepository;
import com.cancun.lastresortavailability.repository.UserRepository;


@ExtendWith(SpringExtension.class)
public class BookingServiceTest {

    @InjectMocks
    private BookingService service;

    @Mock
    private AvailabilityRepository availabilityRepository;

    @Mock
    private BookingRepository bookingRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private BookingValidationService bookingValidationService;

    @Spy
    private List<Availability> availabilities = new ArrayList<>();

    @Captor
    private ArgumentCaptor<Booking> captor;

    @Test
    public void searchAvailability_ok() {
        final BookingMessage bookingMessage = make(BookingMessage.builder().build());

        final User user = make(new User());
        when(userRepository.findById(any())).thenReturn(of(user));

        mockListOfAvailabilities();
        when(availabilityRepository.findByIdInOrderByDayAsc(any())).thenReturn(availabilities);

        service.bookDates(bookingMessage);

        verify(bookingValidationService).validate(bookingMessage, FALSE);
        verify(bookingRepository).save(captor.capture());

        final Booking booking = captor.getValue();
        assertEquals(user, booking.getPax());
    }

    private void mockListOfAvailabilities() {
        final Availability availability = make(new Availability());
        availability.setBooked(TRUE);
        final Availability availability2 = make(new Availability());
        availability2.setBooked(TRUE);
        availabilities.addAll(Arrays.asList(availability, availability2));
    }
}
