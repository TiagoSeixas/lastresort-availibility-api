package com.cancun.lastresortavailability.service;

import static com.cancun.lastresortavailability.fixture.Fixture.make;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.ReflectionTestUtils.setField;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.cancun.lastresortavailability.configuration.BookingConfiguration;
import com.cancun.lastresortavailability.domain.entity.Availability;
import com.cancun.lastresortavailability.domain.entity.Booking;
import com.cancun.lastresortavailability.fixture.Fixture;
import com.cancun.lastresortavailability.repository.AvailabilityRepository;
import com.cancun.lastresortavailability.repository.BookingRepository;


@ExtendWith(MockitoExtension.class)
public class BookingCancellingServiceServiceTest {

    @InjectMocks
    private BookingCancellingService service;

    @Mock
    private AvailabilityRepository availabilityRepository;

    @Mock
    private BookingRepository bookingRepository;

    @Spy
    private List<Availability> availabilities = new ArrayList<>();

    @Test
    public void cancelBooking_ok() {
        final long bookingId = RandomUtils.nextLong();

        final Booking booking = make(new Booking());
        booking.setCancelled(FALSE);
        when(bookingRepository.findById(any())).thenReturn(of(booking));

        mockListOfAvailabilities();

        when(availabilityRepository.findAllByDayIsBetween(any(), any())).thenReturn(availabilities);

        service.cancelBooking(bookingId);

        verify(bookingRepository).save(booking);
        verify(availabilityRepository, times(2)).save(any());
        assertTrue(booking.isCancelled());
    }

    private void mockListOfAvailabilities() {
        final Availability availability = make(new Availability());
        availability.setBooked(TRUE);
        final Availability availability2 = make(new Availability());
        availability2.setBooked(TRUE);
        availabilities.addAll(Arrays.asList(availability, availability2));
    }
}
