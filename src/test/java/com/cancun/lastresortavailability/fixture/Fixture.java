package com.cancun.lastresortavailability.fixture;

import java.time.ZonedDateTime;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.api.Randomizer;

public class Fixture {

    static EasyRandom easyRandom = new EasyRandom();

    public static <T> T make(final T mockClass) {
        return (T) new EasyRandom(
            new EasyRandomParameters()
                .randomize(XMLGregorianCalendar.class, generateXmlGregorianCalendar()))
            .nextObject(mockClass.getClass());
    }

    private static Randomizer<XMLGregorianCalendar> generateXmlGregorianCalendar() {
        return () -> {
            final ZonedDateTime dateTime = ZonedDateTime.now();
            final GregorianCalendar c = GregorianCalendar.from(dateTime);
            try {
                return DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
            } catch (final DatatypeConfigurationException e) {
                e.printStackTrace();
            }
            return null;
        };
    }
}