package com.cancun.lastresortavailability.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "booking")
public class BookingConfiguration {

    private Configuration configuration;

    @Data
    public static class Configuration {

        private Long range;
        private Long maximumDays;
    }
}