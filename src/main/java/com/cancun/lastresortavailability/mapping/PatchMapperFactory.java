package com.cancun.lastresortavailability.mapping;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import net.rakugakibox.spring.boot.orika.OrikaMapperFactoryConfigurer;

@Component
public final class PatchMapperFactory {

    @Bean(name = "patchMapperFacade")
    public static MapperFacade patchMapperFacade(List<? extends OrikaMapperFactoryConfigurer> mappings) {
        DefaultMapperFactory factory = new DefaultMapperFactory.Builder().mapNulls(false).build();
        mappings.forEach(m -> m.configure(factory));
        return factory.getMapperFacade();
    }

    @Primary
    @Bean
    public static MapperFacade mapperFacade(MapperFactory orikaMapperFactory) {
        return orikaMapperFactory.getMapperFacade();
    }
}
