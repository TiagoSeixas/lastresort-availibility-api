package com.cancun.lastresortavailability.mapping;

import org.springframework.stereotype.Component;

import com.cancun.lastresortavailability.domain.dto.BookingDTO;
import com.cancun.lastresortavailability.domain.entity.Booking;

import ma.glasnost.orika.MapperFactory;
import net.rakugakibox.spring.boot.orika.OrikaMapperFactoryConfigurer;

@Component
public class BookingToBookingDTOMapping implements OrikaMapperFactoryConfigurer {

    @Override
    public void configure(final MapperFactory orikaMapperFactory) {
        orikaMapperFactory
            .classMap(Booking.class, BookingDTO.class)
            .field("pax.id", "pax")
            .byDefault()
            .register();
    }
}