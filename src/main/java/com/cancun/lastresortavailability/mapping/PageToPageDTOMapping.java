package com.cancun.lastresortavailability.mapping;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.cancun.lastresortavailability.domain.dto.PageDTO;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.metadata.Type;
import ma.glasnost.orika.metadata.TypeBuilder;
import net.rakugakibox.spring.boot.orika.OrikaMapperFactoryConfigurer;

@Component
public class PageToPageDTOMapping implements OrikaMapperFactoryConfigurer {

    public static final Type<Page<Object>> sourceType = new TypeBuilder<Page<Object>>() {
    }.build();

    public static final Type<PageDTO<Object>> targetType = new TypeBuilder<PageDTO<Object>>() {
    }.build();

    @Override
    public void configure(MapperFactory orikaMapperFactory) {
        orikaMapperFactory
            .classMap(sourceType, targetType)
            .byDefault()
            .customize(new CustomMapper<Page<Object>, PageDTO<Object>>() {
                @Override
                public void mapAtoB(final Page<Object> page, final PageDTO<Object> pageDTO,
                    final MappingContext context) {
                    pageDTO.setSize(page.getSize());
                    pageDTO.setItems(page.getContent());
                }
            })
            .register();
    }
}