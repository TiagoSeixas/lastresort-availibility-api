package com.cancun.lastresortavailability.mapping;

import java.util.List;

import org.springframework.stereotype.Component;

import com.cancun.lastresortavailability.domain.dto.AvailabilityDTO;
import com.cancun.lastresortavailability.domain.entity.Availability;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.metadata.Type;
import ma.glasnost.orika.metadata.TypeBuilder;
import net.rakugakibox.spring.boot.orika.OrikaMapperFactoryConfigurer;

@Component
public class AvailabilityToAvailabilityDTOMapping implements OrikaMapperFactoryConfigurer {

    public static final Type<List<Availability>> sourceType = new TypeBuilder<List<Availability>>() {
    }.build();

    public static final Type<List<AvailabilityDTO>> targetType = new TypeBuilder<List<AvailabilityDTO>>() {
    }.build();

    @Override
    public void configure(MapperFactory orikaMapperFactory) {
        orikaMapperFactory
            .classMap(sourceType, targetType)
            .byDefault()
            .register();
    }
}