package com.cancun.lastresortavailability.domain.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AvailabilityDTO {

    private Long id;
    private LocalDate day;
    private Boolean booked;
}
