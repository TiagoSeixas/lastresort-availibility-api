package com.cancun.lastresortavailability.domain.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PageDTO<T> {

    private List<T> items;
    private Integer size;
}