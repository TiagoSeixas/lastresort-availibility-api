package com.cancun.lastresortavailability.domain.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BookingDTO {

    private Long id;
    private LocalDate checkin;
    private LocalDate checkout;
    private boolean cancelled;
    private Long pax;
}
