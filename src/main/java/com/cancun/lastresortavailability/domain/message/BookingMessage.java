package com.cancun.lastresortavailability.domain.message;

import java.util.List;

import com.cancun.lastresortavailability.domain.entity.Booking;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class BookingMessage {

    private Long pax;
    private List<Long> dateIds;
}
