package com.cancun.lastresortavailability.domain.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
@ToString
@Table(name = "BOOKING", schema = "LAST_RESORT")
public class Booking extends AuditModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column
    private LocalDate checkin;

    @NotNull
    @Column
    private LocalDate checkout;

    @NotNull
    @Column
    private boolean cancelled = false;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User pax;
}
