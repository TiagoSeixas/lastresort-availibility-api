package com.cancun.lastresortavailability.domain.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
@ToString(onlyExplicitlyIncluded = true)
@Table(name = "USER", schema = "LAST_RESORT")
public class User extends AuditModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ToString.Include
    @NotNull(message = "Please set a username to the User")
    @Type(type = "text")
    @Column(name = "username")
    private String username;

    @NotNull(message = "Please set an email to the User")
    @Type(type = "text")
    @Column(name = "email")
    private String email;

    @Type(type = "text")
    @Column(name = "phone")
    private String phone;

    @Column(name = "birthday")
    private LocalDate birthday;

    @NotNull(message = "Please set a password to the User")
    @Type(type = "text")
    @Column(name = "password")
    private String password;

    @Column(name = "enabled")
    private Boolean enabled = true;
}
