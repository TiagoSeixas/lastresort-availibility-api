package com.cancun.lastresortavailability.domain.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "SHEDLOCK", schema = "LAST_RESORT")
public class Shedlock {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String name;

    @Column
    private LocalDateTime lock_until;

    @Column
    private LocalDateTime locked_at;

    @Column
    private String locked_by;

}
