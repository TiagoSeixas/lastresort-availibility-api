package com.cancun.lastresortavailability.web;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.cancun.lastresortavailability.domain.dto.AvailabilityDTO;
import com.cancun.lastresortavailability.domain.dto.BookingDTO;
import com.cancun.lastresortavailability.domain.dto.PageDTO;
import com.cancun.lastresortavailability.domain.entity.Booking;

public interface AvailabilityController {

    @GetMapping("/get-available-days")
    PageDTO<AvailabilityDTO> getAvailableDays();

    @GetMapping("/get-bookings")
    List<BookingDTO> getBookings();

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/book-dates/{userId}")
    void bookDates(@PathVariable final Long userId, @RequestBody final List<Long> dateIds);

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/modify-booking/{bookingId}")
    void modifyBooking(@RequestBody final Booking booking, @PathVariable Long bookingId);

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/cancel-booking/{bookingId}")
    void cancelBooking(@PathVariable final Long bookingId);
}