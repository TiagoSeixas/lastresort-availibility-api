package com.cancun.lastresortavailability.web;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cancun.lastresortavailability.domain.dto.AvailabilityDTO;
import com.cancun.lastresortavailability.domain.dto.BookingDTO;
import com.cancun.lastresortavailability.domain.dto.PageDTO;
import com.cancun.lastresortavailability.domain.entity.Availability;
import com.cancun.lastresortavailability.domain.entity.Booking;
import com.cancun.lastresortavailability.domain.message.BookingMessage;
import com.cancun.lastresortavailability.repository.BookingRepository;
import com.cancun.lastresortavailability.service.AvailabilitySearchService;
import com.cancun.lastresortavailability.service.BookingCancellingService;
import com.cancun.lastresortavailability.service.BookingModifyingService;
import com.cancun.lastresortavailability.service.BookingService;

import lombok.RequiredArgsConstructor;
import ma.glasnost.orika.MapperFacade;

@RestController
@RequiredArgsConstructor
public class LastResortAvailabilityClient implements AvailabilityController {

    private final AvailabilitySearchService service;
    private final BookingService bookingService;
    private final BookingRepository bookingRepository;
    private final BookingCancellingService bookingCancellingService;
    private final BookingModifyingService bookingModifyingService;
    private final MapperFacade mapper;

    @Override
    public PageDTO<AvailabilityDTO> getAvailableDays() {
        final Page<Availability> availabilities = service.getAvailableDays();

        final List<AvailabilityDTO> availabilityDTOS = mapper.mapAsList(
            availabilities.getContent(),
            AvailabilityDTO.class);

        return new PageDTO<AvailabilityDTO>(availabilityDTOS, availabilities.getSize());
    }

    @Override
    public List<BookingDTO> getBookings() {
        return mapper.mapAsList(bookingRepository.findAll(), BookingDTO.class);
    }

    @Override
    public void bookDates(@PathVariable final Long userId, @RequestBody final List<Long> dateIds) {
        final BookingMessage message = BookingMessage.builder()
            .pax(userId)
            .dateIds(dateIds)
            .build();
        bookingService.bookDates(message);
    }

    @Override
    public void modifyBooking(final Booking booking, @PathVariable final Long bookingId) {
        bookingModifyingService.modifyBooking(booking, bookingId);
    }

    @Override
    public void cancelBooking(final Long bookingId) {
        bookingCancellingService.cancelBooking(bookingId);
    }
}
