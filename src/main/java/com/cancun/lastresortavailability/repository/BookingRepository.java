package com.cancun.lastresortavailability.repository;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cancun.lastresortavailability.domain.entity.Booking;

public interface BookingRepository extends JpaRepository<Booking, Long> {

    boolean existsAllByCheckinGreaterThanEqualAndCheckoutLessThanEqual(LocalDate checkin, LocalDate checkout);
}
