package com.cancun.lastresortavailability.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.cancun.lastresortavailability.domain.entity.Availability;

public interface AvailabilityRepository extends JpaRepository<Availability, Long> {

    Optional<Availability> findByDay(LocalDate day);
    List<Availability> findByIdInOrderByDayAsc(List<Long> ids);
    Page<Availability> findAllByDayAfterOrderByDayAsc(LocalDate date, Pageable page);
    List<Availability> findAllByDayIsBetween(LocalDate checkin, LocalDate checkout);
    boolean existsByDay(LocalDate day);
}
