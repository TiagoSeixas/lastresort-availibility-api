package com.cancun.lastresortavailability.util;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.cancun.lastresortavailability.configuration.BookingConfiguration;
import com.cancun.lastresortavailability.service.AvailabilityDataLoadService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequiredArgsConstructor
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private final BookingConfiguration bookingConfigurationConfig;
    private final AvailabilityDataLoadService service;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        log.info("Executing first charge of booking availability based on {} day(s) cycle!",
            bookingConfigurationConfig.getConfiguration().getRange());

        service.generate();
    }
}
