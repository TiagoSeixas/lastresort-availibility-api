package com.cancun.lastresortavailability;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.cancun.lastresortavailability.service.AvailabilityDataLoadService;
import com.netflix.discovery.converters.Auto;

@EnableJpaAuditing
@EnableFeignClients
@SpringBootApplication
public class LastresortAvailabilityApplication {

//    @Autowired
//    AvailabilityDataLoadService service;
//
//    @PostConstruct
//    public void teste() {
//        service.generate();
//    }

    public static void main(String[] args) {
        SpringApplication.run(LastresortAvailabilityApplication.class, args);
    }
}
