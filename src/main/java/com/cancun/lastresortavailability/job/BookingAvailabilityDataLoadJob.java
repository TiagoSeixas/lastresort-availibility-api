package com.cancun.lastresortavailability.job;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.cancun.lastresortavailability.configuration.BookingConfiguration;
import com.cancun.lastresortavailability.domain.entity.Availability;
import com.cancun.lastresortavailability.service.AvailabilityDataLoadService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.javacrumbs.shedlock.core.SchedulerLock;

@Slf4j
@Component
@RequiredArgsConstructor
public class BookingAvailabilityDataLoadJob {

    private final BookingConfiguration bookingConfigurationConfig;
    private final AvailabilityDataLoadService service;

    /**
     * Verifies the range of availability days (configured on a property file) and includes the days of availability
     * (based on the range). It executes as soon as the API runs and every at midnight.
     *
     * @see Availability
     */
    @Scheduled(cron = "${jobs.availability-interval}")
    @SchedulerLock(name = "availability-charge")
    public void execute() {
        log.debug("Executing job to include the booking availability based on {} day(s) cycle!",
            bookingConfigurationConfig.getConfiguration().getRange());

        service.generate();
    }
}