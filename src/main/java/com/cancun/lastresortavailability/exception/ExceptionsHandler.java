package com.cancun.lastresortavailability.exception;

import java.nio.file.AccessDeniedException;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.google.common.collect.ImmutableMap;

@ControllerAdvice
public class ExceptionsHandler extends ResponseEntityExceptionHandler {

    private static final String EXCEPTION = "exception";

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Object> handleAccessDeniedException(final Exception ex, final WebRequest request) {
        return new
            ResponseEntity<>(
            ImmutableMap.of(
                EXCEPTION, createExceptionJson(HttpStatus.FORBIDDEN.value(), "Access denied!")),
            new HttpHeaders(), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(AbstractErrorException.class)
    public ResponseEntity<Object> handleAbstractErrorException(final AbstractErrorException ex, final WebRequest request) {
        return new
            ResponseEntity<>(
            ImmutableMap.of(
                EXCEPTION, createExceptionJson(ex.getErrorType().getStatus().value(), ex.getMessage())),
            new HttpHeaders(), ex.getErrorType().getStatus());
    }

    public Map<String, Object> createExceptionJson(final Integer httpStatus, final String message) {
        return ImmutableMap.of(
            "httpStatus", httpStatus,
            "message", message);
    }

}