package com.cancun.lastresortavailability.exception;

public class ClientErrorException extends AbstractErrorException {

	private static final long serialVersionUID = -6774530400952681241L;

	private final ErrorType errorType;

	public ClientErrorException(final ErrorType errorType, final String message) {
	    super(message);
	    this.errorType = errorType;
    }

    @Override
    public ErrorType getErrorType() {
        return errorType;
    }
}
