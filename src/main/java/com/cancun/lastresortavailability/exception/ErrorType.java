package com.cancun.lastresortavailability.exception;

import static org.springframework.http.HttpStatus.EXPECTATION_FAILED;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.PRECONDITION_FAILED;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorType {

    BOOKING_NOT_FOUND(PRECONDITION_FAILED, "Booking informed was not found!"),
    BOOKING_ALREADY_CANCELLED(PRECONDITION_FAILED, "Booking informed was already cancelled!"),
    INVALID_BOOKING_DATES(EXPECTATION_FAILED, "Booking dates informed are not available for booking!"),
    INVALID_BOOKING_MESSAGE(NOT_ACCEPTABLE, "BookingDate message is invalid!"),
    INVALID_STAY_DURATION(NOT_ACCEPTABLE, "Duration of stay is invalid!"),
    MODIFYING_FOR_THE_SAME_PERIOD(NOT_ACCEPTABLE, "It is not possible to modify a booking to the same period!"),
    NO_AVAILABLE_BOOKING_DATES(PRECONDITION_FAILED, "Booking dates informed are not available for booking!"),
    NO_CONSECUTIVE_BOOKING_DATES(PRECONDITION_FAILED, "Booking days must be consecutive dates!"),
    PAX_NOT_FOUND(PRECONDITION_FAILED, "User informed for booking was not found!");

    private final HttpStatus status;
    private final String message;
}



