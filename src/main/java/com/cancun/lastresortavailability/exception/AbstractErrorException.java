package com.cancun.lastresortavailability.exception;

public abstract class AbstractErrorException extends RuntimeException {

    private static final long serialVersionUID = 5657693474282555089L;

    public AbstractErrorException(final String message) {
        super(message);
    }

    public abstract ErrorType getErrorType();
}
