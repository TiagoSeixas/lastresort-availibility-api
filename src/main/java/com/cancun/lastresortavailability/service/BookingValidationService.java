package com.cancun.lastresortavailability.service;

import static com.cancun.lastresortavailability.exception.ErrorType.INVALID_BOOKING_DATES;
import static com.cancun.lastresortavailability.exception.ErrorType.INVALID_BOOKING_MESSAGE;
import static com.cancun.lastresortavailability.exception.ErrorType.INVALID_STAY_DURATION;
import static com.cancun.lastresortavailability.exception.ErrorType.NO_CONSECUTIVE_BOOKING_DATES;
import static com.cancun.lastresortavailability.exception.ErrorType.PAX_NOT_FOUND;
import static java.lang.Boolean.TRUE;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.Objects.nonNull;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.BooleanUtils.isFalse;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

import java.time.LocalDate;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cancun.lastresortavailability.configuration.BookingConfiguration;
import com.cancun.lastresortavailability.domain.entity.Availability;
import com.cancun.lastresortavailability.domain.message.BookingMessage;
import com.cancun.lastresortavailability.exception.ClientErrorException;
import com.cancun.lastresortavailability.repository.UserRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookingValidationService {

    private final BookingConfiguration bookingConfiguration;
    private final AvailabilitySearchService searchService;
    private final UserRepository userRepository;

    /**
     * Checks if the days requested for booking are available.
     *
     * {@link #isDateInsideAvailableRange(BookingMessage, List)}
     * {@link #isMaximumStay(BookingMessage)}
     * {@link #isConsecutiveDates(BookingMessage, List)}
     * {@link #isDateBooked(BookingMessage, List, boolean)}
     *
     * @see Availability
     */
    public void validate(final BookingMessage message, final boolean modifying) {

        validateMessage(message);

        final List<Availability> availabilityList = searchService.getAvailableDays().getContent();

        final List<Long> availableDates = availabilityList.stream()
            .map(Availability::getId)
            .collect(toList());

        isDateInsideAvailableRange(message, availableDates);

        isMaximumStay(message);

        isConsecutiveDates(message, availabilityList);

        isDateBooked(message, availabilityList, modifying);
    }

    /**
     * Validates the contract message for booking is acceptable.
     */
    private void validateMessage(final BookingMessage message) {
        ofNullable(message)
            .orElseThrow(() -> new ClientErrorException(INVALID_BOOKING_MESSAGE, INVALID_BOOKING_MESSAGE.getMessage()));

        ofNullable(message)
            .map(BookingMessage::getDateIds)
            .map(longs -> longs.get(0))
            .orElseThrow(() -> new ClientErrorException(INVALID_BOOKING_MESSAGE, INVALID_BOOKING_MESSAGE.getMessage()));

        userRepository.findById(message.getPax())
            .orElseThrow(() -> new ClientErrorException(PAX_NOT_FOUND, PAX_NOT_FOUND.getMessage()));
    }

    /**
     * Validates if the dates informed for booking are actually available to bo booked.
     */
    private void isDateInsideAvailableRange(final BookingMessage message, final List<Long> availableDates) {
        final boolean canBeBooked = message.getDateIds().stream()
            .allMatch(availableDates::contains);

        if (isFalse(canBeBooked)) {
            throw new ClientErrorException(INVALID_BOOKING_DATES, INVALID_BOOKING_DATES.getMessage());
        }
    }

    /**
     * Validates if the dates informed for booking respect the maximum stay at the hotel.
     */
    private void isMaximumStay(final BookingMessage message) {
        final boolean canBeBooked =
            message.getDateIds().size() <= bookingConfiguration.getConfiguration().getMaximumDays();

        if (isFalse(canBeBooked)) {
            throw new ClientErrorException(INVALID_STAY_DURATION, INVALID_STAY_DURATION.getMessage());
        }
    }

    /**
     * Validates if the dates informed for booking are consecutive days.
     */
    private void isConsecutiveDates(final BookingMessage message, final List<Availability> availabilityList) {
        final List<LocalDate> datesForBooking = getDatesForBooking(message, availabilityList)
            .stream()
            .map(Availability::getDay)
            .sorted()
            .collect(toList());

        if (datesForBooking.size() > 1) {
            LocalDate previous = null;
            for (final LocalDate bookingDate : datesForBooking) {
                if (nonNull(previous)) {
                    /**
                     * If the difference between dates are more then one day it can't be booked
                     */
                    if (isFalse(DAYS.between(previous, bookingDate) == 1)) {
                        throw new ClientErrorException(NO_CONSECUTIVE_BOOKING_DATES,
                            NO_CONSECUTIVE_BOOKING_DATES.getMessage());
                    }
                }
                previous = bookingDate;
            }
        }
    }

    /**
     * Validates if the dates informed for booking aren't already been booked. If it is modifying a booking, it ignores
     * booking from the same pax.
     */
    private void isDateBooked(final BookingMessage message, final List<Availability> availabilityList,
        final boolean modifying) {

        final boolean alreadyBooked = getDatesForBooking(message, availabilityList).stream()
            .anyMatch(availability -> isTrue(availability.getBooked()) &&
                modifying ? isFalse(availability.getPax().getId() == message.getPax()) : TRUE);

        if (isTrue(alreadyBooked)) {
            throw new ClientErrorException(INVALID_BOOKING_DATES, INVALID_BOOKING_DATES.getMessage());
        }
    }

    private List<Availability> getDatesForBooking(final BookingMessage message,
        final List<Availability> availabilityList) {
        return availabilityList.stream()
            .filter(availability -> message.getDateIds().stream()
                .anyMatch(id -> availability.getId().equals(id)))
            .collect(toList());
    }
}
