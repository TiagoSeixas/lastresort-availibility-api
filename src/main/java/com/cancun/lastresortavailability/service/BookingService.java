package com.cancun.lastresortavailability.service;

import static com.cancun.lastresortavailability.exception.ErrorType.PAX_NOT_FOUND;
import static java.lang.Boolean.TRUE;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.cancun.lastresortavailability.domain.entity.Availability;
import com.cancun.lastresortavailability.domain.entity.Booking;
import com.cancun.lastresortavailability.domain.entity.User;
import com.cancun.lastresortavailability.domain.message.BookingMessage;
import com.cancun.lastresortavailability.exception.ClientErrorException;
import com.cancun.lastresortavailability.repository.AvailabilityRepository;
import com.cancun.lastresortavailability.repository.BookingRepository;
import com.cancun.lastresortavailability.repository.UserRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookingService {

    private final AvailabilityRepository availabilityRepository;
    private final BookingRepository bookingRepository;
    private final UserRepository userRepository;
    private final BookingValidationService bookingValidationService;

    /**
     * Books a stay at the hotel.
     *
     * @param message a message informing the days' ids and the user id used on reservation
     * @see BookingMessage
     */
    public void bookDates(final BookingMessage message) {

        bookingValidationService.validate(message, false);

        final User pax = userRepository.findById(message.getPax())
            .orElseThrow(() -> new ClientErrorException(PAX_NOT_FOUND, PAX_NOT_FOUND.getMessage()));

        final List<Availability> availabilities = availabilityRepository.findByIdInOrderByDayAsc(message.getDateIds());

        final Optional<LocalDate> checkin = availabilities.stream()
            .sorted(Comparator.comparing(Availability::getDay))
            .map(Availability::getDay)
            .findFirst();

        final Optional<LocalDate> checkout = availabilities.stream()
            .sorted((a, b) -> b.getDay().compareTo(a.getDay()))
            .map(Availability::getDay)
            .findFirst();

        final Booking booking = new Booking();
        booking.setPax(pax);
        booking.setCheckin(checkin.get());
        booking.setCheckout(checkout.get());
        bookingRepository.save(booking);

        log.debug("A reservation was made by: {}. Check-in: {}, Check-out: {}.", pax, checkin.get(), checkout.get());

        availabilities.forEach(av -> {
                av.setBooked(TRUE);
                av.setPax(pax);
                availabilityRepository.save(av);
            });
    }
}
