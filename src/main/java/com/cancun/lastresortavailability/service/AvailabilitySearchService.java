package com.cancun.lastresortavailability.service;

import static com.cancun.lastresortavailability.exception.ErrorType.NO_AVAILABLE_BOOKING_DATES;
import static java.util.Optional.of;

import java.time.LocalDate;
import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.cancun.lastresortavailability.configuration.BookingConfiguration;
import com.cancun.lastresortavailability.domain.entity.Availability;
import com.cancun.lastresortavailability.domain.entity.Booking;
import com.cancun.lastresortavailability.exception.ClientErrorException;
import com.cancun.lastresortavailability.repository.AvailabilityRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class AvailabilitySearchService {

    private final AvailabilityRepository repository;
    private final BookingConfiguration configuration;

    /**
     * Searches the availability period.
     *
     * @see Availability
     * @return a page of availabilities based on a range configuration (default 30 days)
     */
    public Page<Availability> getAvailableDays() {

        final Long bookingRange = configuration.getConfiguration().getRange();
        log.debug("Searching available booking dates based on {} day(s) range.", bookingRange);

        final Pageable page = PageRequest.of(0, bookingRange.intValue());
        final Page<Availability> availabilities = repository.findAllByDayAfterOrderByDayAsc(LocalDate.now(), page);

        of(availabilities.getContent())
            .map(Collection::iterator)
            .orElseThrow(() -> new ClientErrorException(
                NO_AVAILABLE_BOOKING_DATES,
                NO_AVAILABLE_BOOKING_DATES.getMessage()));

        return availabilities;
    }
}
