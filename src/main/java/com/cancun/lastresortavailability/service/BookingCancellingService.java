package com.cancun.lastresortavailability.service;

import static com.cancun.lastresortavailability.exception.ErrorType.BOOKING_ALREADY_CANCELLED;
import static com.cancun.lastresortavailability.exception.ErrorType.BOOKING_NOT_FOUND;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

import java.time.LocalDate;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cancun.lastresortavailability.domain.entity.Availability;
import com.cancun.lastresortavailability.domain.entity.Booking;
import com.cancun.lastresortavailability.exception.ClientErrorException;
import com.cancun.lastresortavailability.repository.AvailabilityRepository;
import com.cancun.lastresortavailability.repository.BookingRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookingCancellingService {

    private final AvailabilityRepository availabilityRepository;
    private final BookingRepository bookingRepository;

    /**
     * Cancel a booking from the hotel.
     *
     * @param bookingId the id of the booking to be cancelled.
     * @see Booking
     */
    public void cancelBooking(final Long bookingId) {

        final Booking booking = getBooking(bookingId);
        verifyIfCancelled(booking);

        booking.setCancelled(TRUE);
        bookingRepository.save(booking);

        final LocalDate checkin = booking.getCheckin();
        final LocalDate checkout = booking.getCheckout();

        final List<Availability> reservationDays = availabilityRepository.findAllByDayIsBetween(checkin, checkout);

        reservationDays.stream()
            .forEach(av -> {
                av.setBooked(FALSE);
                availabilityRepository.save(av);
            });

        log.debug("The reservation: {} was cancelled!", booking);
    }

    /**
     * Gets a booking referred by it's id and verify if it exists.
     *
     * @param bookingId id of the booking
     * @see Booking
     */
    private Booking getBooking(final Long bookingId) {
        final Booking booking = bookingRepository.findById(bookingId)
            .orElseThrow(() -> new ClientErrorException(BOOKING_NOT_FOUND, BOOKING_NOT_FOUND.getMessage()));
        return booking;
    }

    /**
     * Verifies if booking informed is already cancelled.
     *
     * @param booking an booking object to be checked
     * @see Booking
     */
    private void verifyIfCancelled(final Booking booking) {
        if (booking.isCancelled())
            throw new ClientErrorException(BOOKING_ALREADY_CANCELLED, BOOKING_ALREADY_CANCELLED.getMessage());
    }
}
