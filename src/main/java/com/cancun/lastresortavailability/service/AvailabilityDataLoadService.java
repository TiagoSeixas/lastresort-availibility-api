package com.cancun.lastresortavailability.service;

import static java.lang.Boolean.FALSE;
import static org.apache.commons.lang.BooleanUtils.isFalse;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.cancun.lastresortavailability.configuration.BookingConfiguration;
import com.cancun.lastresortavailability.domain.entity.Availability;
import com.cancun.lastresortavailability.repository.AvailabilityRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AvailabilityDataLoadService {

    private final AvailabilityRepository repository;
    private final BookingConfiguration bookingConfigurationConfig;

    /**
     * Includes a first load at the database including available days to bo booked
     * based on a range configuration (default 30 days).
     *
     * @see Availability
     */
    public void generate() {
        final LocalDate bookingStart = LocalDate.now().plusDays(1L);

        final Long days = bookingConfigurationConfig.getConfiguration().getRange();

        final List<LocalDate> bookingRange = Stream.iterate(bookingStart, d -> d.plusDays(1))
            .limit(days)
            .collect(Collectors.toList());

        bookingRange.forEach(day -> {
            if (isFalse(repository.existsByDay(day))) {
                final Availability availability = new Availability();
                availability.setBooked(FALSE);
                availability.setDay(day);
                repository.save(availability);
            }
        });
    }
}
