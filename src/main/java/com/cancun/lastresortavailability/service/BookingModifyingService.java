package com.cancun.lastresortavailability.service;

import static com.cancun.lastresortavailability.exception.ErrorType.BOOKING_ALREADY_CANCELLED;
import static com.cancun.lastresortavailability.exception.ErrorType.BOOKING_NOT_FOUND;
import static com.cancun.lastresortavailability.exception.ErrorType.MODIFYING_FOR_THE_SAME_PERIOD;
import static com.cancun.lastresortavailability.exception.ErrorType.NO_AVAILABLE_BOOKING_DATES;
import static org.apache.commons.lang.BooleanUtils.isTrue;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.stereotype.Service;

import com.cancun.lastresortavailability.domain.entity.Availability;
import com.cancun.lastresortavailability.domain.entity.Booking;
import com.cancun.lastresortavailability.domain.message.BookingMessage;
import com.cancun.lastresortavailability.exception.ClientErrorException;
import com.cancun.lastresortavailability.exception.ErrorType;
import com.cancun.lastresortavailability.repository.AvailabilityRepository;
import com.cancun.lastresortavailability.repository.BookingRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookingModifyingService {

    private final BookingValidationService bookingValidationService;
    private final BookingService bookingService;
    private final BookingRepository bookingRepository;
    private final BookingCancellingService bookingCancellingService;
    private final AvailabilityRepository availabilityRepository;

    /**
     * Modify a booking. It is transactional because it can't modify anything if there is an error.
     *
     * @param booking the Booking object requested to modify.
     * @see Booking
     */
    public void modifyBooking(final Booking booking, final Long bookingId) {

        final Booking actualBooking = bookingRepository.findById(bookingId)
            .orElseThrow(() -> new ClientErrorException(BOOKING_NOT_FOUND, BOOKING_NOT_FOUND.getMessage()));

        validateAlreadyCancelledBooking(actualBooking);
        validateModifyingToDifferentPeriod(booking, actualBooking);

        final BookingMessage message = createBookingMessage(booking);

        bookingValidationService.validate(message, true);

        bookingCancellingService.cancelBooking(bookingId);

        bookingService.bookDates(message);

        log.debug("The reservation: {} was modified!", bookingId);
    }

    private void validateAlreadyCancelledBooking(final Booking actualBooking) {
        if (isTrue(actualBooking.isCancelled())) {
            throw new ClientErrorException(BOOKING_ALREADY_CANCELLED, BOOKING_ALREADY_CANCELLED.getMessage());
        }
    }

    private void validateModifyingToDifferentPeriod(final Booking booking, final Booking actualBooking) {
        if (isTrue(actualBooking.getCheckin().equals(booking.getCheckin()) &&
            actualBooking.getCheckout().equals(booking.getCheckout()))) {
            throw new ClientErrorException(MODIFYING_FOR_THE_SAME_PERIOD, MODIFYING_FOR_THE_SAME_PERIOD.getMessage());
        }
    }

    private BookingMessage createBookingMessage(final Booking booking) {
        final Availability availabilityStart = availabilityRepository.findByDay(booking.getCheckin())
            .orElseThrow(
                () -> new ClientErrorException(NO_AVAILABLE_BOOKING_DATES, NO_AVAILABLE_BOOKING_DATES.getMessage()));

        final Availability availabilityEnd = availabilityRepository.findByDay(booking.getCheckout())
            .orElseThrow(
                () -> new ClientErrorException(NO_AVAILABLE_BOOKING_DATES, NO_AVAILABLE_BOOKING_DATES.getMessage()));

        final List<Long> dateIds = availabilityRepository
            .findAllByDayIsBetween(availabilityStart.getDay(), availabilityEnd.getDay()).stream()
            .map(Availability::getId)
            .collect(Collectors.toList());

        final BookingMessage message = BookingMessage.builder()
            .dateIds(dateIds)
            .pax(booking.getPax().getId())
            .build();
        return message;
    }
}
